public class UCBPolicy extends AbstractPolicy {

    public UCBPolicy(Arm[] arms, int times) {
        super(arms, times);
    }

    @Override
    public PolicyResult execute(int indexOfOptimalArm, double averageRewardOfBestArm) {
        int count = 0;
        for (; count < this.numberOfArms; count++) {
            pullArm(count);
        }

        for (; count < this.times; count++) {
            double[] UCBs = calculateUCB(count);
            int selectedArm = findIndexOfHighestValue(UCBs);
            pullArm(selectedArm);
        }
        return getResult(averageRewardOfBestArm);
    }

    private double[] calculateUCB(int count) {
        double[] UCBs = new double[this.numberOfArms];
        for (int i = 0; i < this.numberOfArms; i++) {
            UCBs[i] = this.averageReward[i] + Math.sqrt(Math.log(count) / this.selectionHistory[i]);
        }
        return UCBs;
    }
}
