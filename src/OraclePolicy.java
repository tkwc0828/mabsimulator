public class OraclePolicy extends AbstractPolicy {

    private int indexOfOptimalArm;

    public OraclePolicy(Arm[] arms, int times) {
        super(arms, times);
    }

    @Override
    public PolicyResult execute(int indexOfOptimalArm, double averageRewardOfBestArm) {
        this.indexOfOptimalArm = indexOfOptimalArm;
        for (int i = 0; i < this.times; i++) {
            pullArm(indexOfOptimalArm);
        }
         return getResult();
    }

    private PolicyResult getResult() {
        return new PolicyResult(averageReward[indexOfOptimalArm], 0, selectionHistory);
    }
}
