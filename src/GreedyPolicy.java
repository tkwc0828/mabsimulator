public class GreedyPolicy extends AbstractPolicy {

    private double explorationRatio;

    public GreedyPolicy(Arm[] arms, int times, double explorationRatio) {
        super(arms, times);
        this.explorationRatio = explorationRatio;
    }

    @Override
    public PolicyResult execute(int indexOfOptimalArm, double averageRewardOfBestArm) {
        int exploration = (int) Math.round(this.times * this.explorationRatio);

        for (int i = 0; i < exploration; i++) {
            pullRandomArm();
        }

        int indexOfBestArm = findIndexOfHighestValue(this.averageReward);
        for (int i = exploration; i < this.times; i++) {
            pullArm(indexOfBestArm);
        }
        return getResult(averageRewardOfBestArm);
    }
}
