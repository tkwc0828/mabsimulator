import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        int times = 10000;
        int m = 100;
        int numberOfArms = 4;
        int[] reward = {0, 1};
        double probability1= 0.1;
        double probability2= 0.2;
        double probability3= 0.3;
        double probability4= 0.4;
        double[][] probability = {
                {1-probability1, probability1},
                {1-probability2, probability2},
                {1-probability3, probability3},
                {1-probability4, probability4}
        };
        Integer[] seeds = {0, 1, 2, 3};
        int indexOfOptimalArm = 3;
        double explorationRatio = 0.002;
        double epsilon = 0.2;

        PolicyResult finalOraclePolicyResult = new PolicyResult(numberOfArms);
        PolicyResult finalGreedyPolicyResult = new PolicyResult(numberOfArms);
        PolicyResult finalEpsilonGreedyPolicyResult = new PolicyResult(numberOfArms);
        PolicyResult finalUCBPolicyResult = new PolicyResult(numberOfArms);

        for (int i = 0; i < m; i++) {
            final int count = i;
            ArrayList<Integer> seedsArrayList = Arrays.stream(seeds).map(j -> j + 4 * (count + 1)).collect(Collectors.toCollection(ArrayList::new));
            Arm[] arms = Arm.getFourArms(reward, probability, seedsArrayList);

            OraclePolicy oraclePolicy = new OraclePolicy(arms, times);
            PolicyResult oraclePolicyResult = oraclePolicy.execute(indexOfOptimalArm, times);
            finalOraclePolicyResult.accumulateResult(oraclePolicyResult);
            double averageRewardOfBestArm = oraclePolicyResult.getAverageReward();

            GreedyPolicy greedyPolicy = new GreedyPolicy(arms, times, explorationRatio);
            PolicyResult greedyPolicyResult = greedyPolicy.execute(indexOfOptimalArm, averageRewardOfBestArm);
            finalGreedyPolicyResult.accumulateResult(greedyPolicyResult);

            EpsilonGreedyPolicy epsilonGreedyPolicy = new EpsilonGreedyPolicy(arms, times, epsilon);
            PolicyResult epsilonGreedyPolicyResult = epsilonGreedyPolicy.execute(indexOfOptimalArm, averageRewardOfBestArm);
            finalEpsilonGreedyPolicyResult.accumulateResult(epsilonGreedyPolicyResult);

            UCBPolicy ucbPolicy = new UCBPolicy(arms, times);
            PolicyResult ucbPolicyResult = ucbPolicy.execute(indexOfOptimalArm, averageRewardOfBestArm);
            finalUCBPolicyResult.accumulateResult(ucbPolicyResult);
        }

        finalOraclePolicyResult.printResult(indexOfOptimalArm, "Oracle Policy");
        finalGreedyPolicyResult.printResult(indexOfOptimalArm, "Greedy Policy");
        finalEpsilonGreedyPolicyResult.printResult(indexOfOptimalArm, "Epsilon Greedy Policy");
        finalUCBPolicyResult.printResult(indexOfOptimalArm, "UCB Policy");
    }
}
