import java.util.Random;

public class EpsilonGreedyPolicy extends AbstractPolicy {

    private double epsilon;
    private double[] epsilonDistribution = new double[2];

    public EpsilonGreedyPolicy(Arm[] arms, int times, double epsilon) {
        super(arms, times);
        this.epsilon = epsilon;
        this.epsilonDistribution[0] = this.epsilon;
        this. epsilonDistribution[1] = 1 - this.epsilon;
    }

    @Override
    public PolicyResult execute(int indexOfOptimalArm, double averageRewardOfBestArm) {
        for (int i = 0; i < this.times; i++) {
            int selectedArm = 0;
            if (isSelectBestArm()) {
                selectedArm = findIndexOfHighestValue(this.averageReward);
                pullArm(selectedArm);
            } else {
                pullRandomArm();
            }
        }
        return getResult(averageRewardOfBestArm);
    }

    private boolean isSelectBestArm() {
        double randomNumber = new Random().nextDouble();
        if (randomNumber < this.epsilonDistribution[0]) {
            return false;
        }
        return true;
    }
}
