import java.util.Arrays;
import java.util.Random;
import java.util.stream.DoubleStream;

public abstract class AbstractPolicy {
    int times;
    int numberOfArms;
    int[] selectionHistory;
    double[] averageReward;
    double[] rewardHistory;
    Arm[] arms;

    public AbstractPolicy(Arm[] arms, int times) {
        this.arms = arms;
        this.times = times;
        this.numberOfArms = arms.length;
        this.selectionHistory = new int[numberOfArms];
        this.rewardHistory = new double[numberOfArms];
        this.averageReward = new double[numberOfArms];
        Arm.resetSeed(this.arms);
    }

    public abstract PolicyResult execute(int indexOfOptimalArm, double averageRewardOfBestArm);

    public void pullArm(int selectedArm) {
        int reward = this.arms[selectedArm].genReward();
        this.rewardHistory[selectedArm] = this.rewardHistory[selectedArm] + reward;
        this.selectionHistory[selectedArm]++;
        this.averageReward[selectedArm] = this.rewardHistory[selectedArm] / this.selectionHistory[selectedArm];
    }

    public void pullRandomArm() {
        int selectedArm = new Random().nextInt(4);
        int reward = this.arms[selectedArm].genReward();
        this.rewardHistory[selectedArm] = this.rewardHistory[selectedArm] + reward;
        this.selectionHistory[selectedArm]++;
        this.averageReward[selectedArm] = this.rewardHistory[selectedArm] / this.selectionHistory[selectedArm];
    }

    public int findIndexOfHighestValue(double[] array) {
        int indexOfHighestValue = 0;
        double maxValue = 0;
        for (int i = 0; i < array.length; i++) {
            double value = array[i];
            if (value <= maxValue) {
                   continue;
            }
            maxValue = value;
            indexOfHighestValue = i;
        }
        return indexOfHighestValue;
    }

    public void printArmsResult() {
        System.out.println("Reward of each arm: " + Arrays.toString(this.rewardHistory));
        System.out.println("Number of times of each arm: " + Arrays.toString(this.selectionHistory));
        System.out.println("Average of each arm: " + Arrays.toString(this.averageReward));
        System.out.println("\n");
    }

    public PolicyResult getResult(double averageRewardOfBestArm) {
        double averageReward = DoubleStream.of(rewardHistory).sum() / this.times;
        double averageRegard = averageRewardOfBestArm - averageReward;
        return new PolicyResult(averageReward, averageRegard, selectionHistory);
    }
}
