import java.util.ArrayList;
import java.util.Random;

public class Arm {
    private int[] reward;
    private double[] distribution;
    private Random rand;
    private long seed;

    private Arm(int[] reward, double[] probability, long seed) {
        this.reward = reward;
        this.distribution = new double[probability.length];
        double sum = 0;
        for (int i = 0; i < probability.length ; i++) {
            sum += probability[i];
            this.distribution[i] = sum;
        }
        this.seed = seed;
        this.rand = new Random(seed);
    }

    public int genReward() {
        double randomNumber = this.rand.nextDouble();
        int index = 0;
        for (; index < this.distribution.length ; index++) {
            if (randomNumber < this.distribution[index]) {
                break;
            }
        }
        return this.reward[index];
    }

    public static Arm[] getFourArms(int[] reward, double[][] probability, ArrayList<Integer> seeds) {
        int numberOfArms = probability.length;
        Arm[] arms = new Arm[numberOfArms];
        for (int i = 0; i < numberOfArms; i++) {
            arms[i] = new Arm(reward, probability[i], seeds.get(i));
        }
        return arms;
    }

    public static void resetSeed(Arm[] arms) {
        for (int i = 0; i < arms.length; i++) {
            Arm arm = arms[i];
            arm.getRand().setSeed(arm.getSeed());
        }
    }

    public Random getRand() {
        return rand;
    }

    public long getSeed() {
        return seed;
    }
}
