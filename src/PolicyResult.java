import java.util.Arrays;
import java.util.stream.IntStream;

public class PolicyResult {
    private double averageReward;
    private double averageRegard;
    private int[] selectionHistory;

    public PolicyResult(int numberOfArms) {
        this.averageReward = 0;
        this.averageRegard = 0;
        this.selectionHistory = new int[numberOfArms];
    }

    public PolicyResult(double averageReward, double averageRegard, int[] selectionHistory) {
        this.averageReward = averageReward;
        this.averageRegard = averageRegard;
        this.selectionHistory = selectionHistory;
    }

    public void accumulateResult(PolicyResult policyResult) {
        if (this.averageReward == 0) {
            this.averageReward = policyResult.getAverageReward();
            this.averageRegard = policyResult.getAverageRegard();
            this.selectionHistory = policyResult.getSelectionHistory();
        } else {
            this.averageReward = (this.averageReward + policyResult.getAverageReward()) / 2;
            this.averageRegard = (this.averageRegard + policyResult.getAverageRegard()) / 2;
            Arrays.setAll(this.selectionHistory, i -> this.selectionHistory[i] + policyResult.getSelectionHistory()[i]);
        }
    }

    public void printResult(int indexOfOptimalArm, String policy) {
        System.out.println("Average Reward of " + policy + ": " + this.averageReward);
        System.out.println("Average Regard of " + policy + ": " + this.averageRegard);
        System.out.println("Percentage Of Optimal of " + policy + ": " + getPercentageOfOptimal(indexOfOptimalArm));
        System.out.println("Number of times of each arms: " + Arrays.toString(this.selectionHistory));
        System.out.println();
    }

    private double getPercentageOfOptimal(int indexOfOptimalArm) {
        return (double) this.selectionHistory[indexOfOptimalArm] / IntStream.of(selectionHistory).sum() * 100;
    }

    public double getAverageReward() {
        return averageReward;
    }

    public void setAverageReward(double averageReward) {
        this.averageReward = averageReward;
    }

    public double getAverageRegard() {
        return averageRegard;
    }

    public void setAverageRegard(double averageRegard) {
        this.averageRegard = averageRegard;
    }

    public int[] getSelectionHistory() {
        return selectionHistory;
    }

    public void setSelectionHistory(int[] selectionHistory) {
        this.selectionHistory = selectionHistory;
    }
}
